<?php

namespace App\Http\Controllers\API\UserFriend;

use App\Enums\StudentCourseStatusEnum;
use App\Enums\UserFriendshipStatusEnum;
use App\Http\Requests\API\CommonRequests\ArrayIntAPIRequest;
use App\Http\Requests\API\UserFriend\ChangeUserFriendshipStatusAPIRequest;
use App\Http\Requests\API\UserFriend\CreateUserFriendAPIRequest;
use App\Http\Requests\API\UserFriend\UpdateUserFriendAPIRequest;
use App\Models\User;
use App\Models\UserFriend;
use App\Models\UserFriendshipStatus;
use App\Notifications\FriendRequest;
use App\Notifications\FriendshipAccepted;
use App\Repositories\UserFriendRepository;
use App\Repositories\UserFriendshipStatusRepository;
use App\Services\UserFriend\UserFriendService;
use App\Services\UserSettingsService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;

/**
 * Class UserFriendController
 * @package App\Http\Controllers\API
 */

class UserFriendAPIController extends AppBaseController
{
    /** @var  UserFriendRepository */
    private $userFriendRepo;
    private $userFriendService;
    private $userFriendshipStatusRepo;
    private $userSettingsService;

    public function __construct(
        UserFriendRepository $userFriendRepo,
        UserFriendService $userFriendService,
        UserFriendshipStatusRepository $userFriendshipStatusRepo,
        UserSettingsService $userSettingsService
    )
    {
        $this->userFriendRepo = $userFriendRepo;
        $this->userFriendService = $userFriendService;
        $this->userFriendshipStatusRepo = $userFriendshipStatusRepo;
        $this->userSettingsService = $userSettingsService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @SWG\Get(
     *      path="/user_friends",
     *      summary="Get a listing of the UserFriend.",
     *      tags={"UserFriend"},
     *      description="Get all UserFriend",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/UserFriend")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request): JsonResponse
    {
        $userFriends = $this->userFriendRepo->queryByRequest($request)->get();

        return $this->sendResponse($userFriends->toArray(), 'User Friends retrieved successfully');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @SWG\Get(
     *      path="/my_friends",
     *      summary="Get a listing of the Friends.",
     *      tags={"UserFriend"},
     *      description="Get all UserFriend",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/UserFriend")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function getMyFriends(Request $request): JsonResponse
    {
        $userFriends = $this->userFriendService->getFriendsByUserId(Auth::id(), $request);

        return $this->sendResponse($userFriends->toArray(), 'User Friends retrieved successfully');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @SWG\Get(
     *      path="/my_friend_requests_as_addressee",
     *      summary="Get a listing of the Friends.",
     *      tags={"UserFriend"},
     *      description="Get all UserFriend",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/UserFriend")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function getMyRequestsAsAddressee(Request $request): JsonResponse
    {
        $userId = Auth::id();

        $userFriends = $this->userFriendService->getByAddressee($request, $userId);

        return $this->sendResponse($userFriends->toArray(), 'User Friends retrieved successfully');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @SWG\Get(
     *      path="/my_friend_requests_as_requester",
     *      summary="Get a listing of the Friends.",
     *      tags={"UserFriend"},
     *      description="Get all UserFriend",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/UserFriend")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function getMyRequestsAsRequester(Request $request): JsonResponse
    {
        $userId = Auth::id();

        $userFriends = $this->userFriendService->getByRequester($request, $userId);

        return $this->sendResponse($userFriends->toArray(), 'User Friends retrieved successfully');
    }

    /**
     * @param int $addresseeId
     * @return JsonResponse
     *
     * @SWG\Post(
     *      path="/users/{addressee_id}/friend_request",
     *      summary="Request for the user you want to add as a friend",
     *      tags={"UserFriend"},
     *      description="Store UserFriend",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="addressee_id",
     *          description="id of User you want to add as a friend",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/UserFriend"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function friendRequest(int $addresseeId): JsonResponse
    {
        if ($addresseeId == Auth::id()) {
            return $this->sendError("You cannot add yourself to friends");
        }

        $userAddressee = User::find($addresseeId, ['id']);

        if (empty($userAddressee)) {
            return $this->sendError('User not found');
        }

        $isAllowFriendship = $this->userSettingsService->isAllowFriendshipByUser($userAddressee->id);

        if (!$isAllowFriendship) {
            return $this->sendError('User does not allow friendship');
        }

        $userFriend = $this->userFriendService->findByUsers(Auth::id(), $addresseeId);

        if (!empty($userFriend)) {
            return $this->sendError('User already in friends');
        }

        $statusInReviewId = $this->userFriendshipStatusRepo->getStatusBySlug(UserFriendshipStatusEnum::SLUG_IN_REVIEW, true);

        if (is_null($statusInReviewId)) {
            return $this->sendError('System error: status not found');
        }

        $userAddressee->notify(new FriendRequest(Auth::user()));
        $this->userFriendRepo->createBy(Auth::id(), $userAddressee->id, $statusInReviewId);

        return $this->sendSuccess('Request sent successfully');
    }

    /**
     * @param int $requesterId
     * @param Request $request
     * @return JsonResponse
     *
     * @SWG\Put(
     *      path="/my_friend_requests/{requester_id}",
     *      summary="Request for the user_friend you want to change status",
     *      tags={"UserFriend"},
     *      description="Store UserFriend",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="requester_id",
     *          description="id of the User who requested you to be a friend",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="user_friendship_status_id",
     *          description="id of UserFriendshipStatus",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/UserFriend"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function changeStatusForFriendRequest(int $requesterId, ChangeUserFriendshipStatusAPIRequest $request): JsonResponse
    {
        /** @var User $addressee */
        $addressee = Auth::user();
        $userFriend = $this->userFriendRepo->getOneBy($requesterId, $addressee->id);

        if (empty($userFriend)) {
            return $this->sendError('Friend request not found');
        }

        $friendshipStatusId = $request->input('user_friendship_status_id');
        $friendshipStatus = UserFriendshipStatus::find($friendshipStatusId, ['id', 'status_slug']);
        if ($friendshipStatus->status_slug == UserFriendshipStatusEnum::SLUG_ACCEPT) {
            /** @var User $requester */
            $requester = User::find($requesterId, ['id', 'user_first_name', 'user_last_name']);
            $requester->notify(new FriendshipAccepted($addressee));
        }
        $this->userFriendRepo->updateStatusBy($requesterId, $addressee->id, $friendshipStatus->id);

        return $this->sendSuccess('Friend request status changed successfully');
    }

    /**
     * @param ArrayIntAPIRequest $request
     * @return JsonResponse
     *
     * @SWG\Delete (
     *      path="/my_friends/",
     *      summary="Remove the specified UserFriend from storage",
     *      tags={"UserFriend"},
     *      description="Delete UserFriend",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroyBulk(ArrayIntAPIRequest $request): JsonResponse
    {
        $ids = $request->input('data');

        $this->userFriendRepo->deleteFriendsByUserIds(Auth::id(), $ids);

        return $this->sendSuccess('User Friends deleted successfully');
    }
}
