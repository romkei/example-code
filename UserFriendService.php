<?php

namespace App\Services\UserFriend;

use App\Repositories\UserFriendRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class UserFriendService
{
    private $userFriendRepo;

    public function __construct(
        UserFriendRepository $userFriendRepo
    ) {
        $this->userFriendRepo = $userFriendRepo;
    }

    public function getByAddressee($request, int $addresseeId): Collection
    {
        return $this->userFriendRepo->getBy($request, ['addressee_id' => $addresseeId]);
    }

    public function getByRequester($request, int $requesterId): Collection
    {
        return $this->userFriendRepo->getBy($request, ['requester_id' => $requesterId]);
    }

    public function getFriendsByUserId(int $userId, Request $request): Collection
    {
        return $this->userFriendRepo->getFriendsByUserId($userId, $request);
    }

    public function findByUsers(int $userOneId, int $userTwoId)
    {
        return $this->userFriendRepo->findByUsers($userOneId, $userTwoId);
    }
}
