<?php

namespace App\Http\Controllers\API\Dashboard;

use App\Enums\DashboardWidgetSlugsEnum;
use App\Repositories\Dashboard\UserDashboardWidgetRepository;
use App\Repositories\RoleRepository;
use App\Repositories\UserOrganizationRepository;
use App\Services\DashboardService\DashboardService;
use App\Models\{Dashboard\DashboardWidget, Role};
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Illuminate\Support\Facades\Auth;

/**
 * Class AssessmentTypeController
 * @package App\Http\Controllers\API
 */

class DashboardAPIController extends AppBaseController
{
    private $dashboardService;
    private $userOrganizationRepo;

    public function __construct(
        DashboardService $dashboardService,
        UserOrganizationRepository $userOrganizationRepo
    )
    {
        $this->dashboardService = $dashboardService;
        $this->userOrganizationRepo = $userOrganizationRepo;
    }

    /**
     * @return JsonResponse
     *
     * @SWG\Get(
     *      path="/dashboard/counted_statistic",
     *      summary="Get counted statistic for dashboard",
     *      tags={"Dashboard"},
     *      description="Get data for dashboard",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(
     *                      type="object",
     *                      @SWG\Property(property="id", type="integer"),
     *                      @SWG\Property(property="name", type="string"),
     *                      @SWG\Property(property="value", type="integer"),
     *                  )
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function getCounted(): JsonResponse
    {
        $widgets = $this->dashboardService->widgetsByCurrentUser();
        $organizationIds = getOrganizationId(true);
        $response = [];

        $widgets->each(function (DashboardWidget $widget) use (&$response, $organizationIds) {
            $widgetResp = function ($value) use ($widget) {
                return [
                    'id' => $widget->id,
                    'name' => $widget->widget_name,
                    'value' => $value,
                ];
            };
            switch ($widget->widget_slug) {
                case DashboardWidgetSlugsEnum::COURSES_COUNT:
                    $response[] = $widgetResp($this->dashboardService->coursesCount($organizationIds));
                    break;
                case DashboardWidgetSlugsEnum::STUDENTS_COUNT:
                    $response[] = $widgetResp($this->dashboardService->usersCountByRoleName(Role::ROLE_STUDENT, $organizationIds));
                    break;
                case DashboardWidgetSlugsEnum::TEACHERS_COUNT:
                    $response[] = $widgetResp($this->dashboardService->usersCountByRoleName(Role::ROLE_TEACHER, $organizationIds));
                    break;
                case DashboardWidgetSlugsEnum::MY_COURSES_COUNT:
                    $response[] = $widgetResp($this->dashboardService->myTeachingCoursesCount($organizationIds));
                    break;
                case DashboardWidgetSlugsEnum::STUDENTS_ONLINE_COUNT:
                    $response[] = $widgetResp($this->dashboardService->usersOnlineCountByRoleName(Role::ROLE_STUDENT, $organizationIds));
                    break;
            }
        });

        return $this->sendResponse($response, 'Courses count retrieved successfully');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @SWG\Get(
     *      path="/dashboard/courses_teaching",
     *      summary="Get counted statistic for dashboard",
     *      tags={"Dashboard"},
     *      description="Get data for dashboard",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="organization_id",
     *          description="id of Organizaiton",
     *          type="integer",
     *          required=false,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="object",
     *                  @SWG\Property(property="id", type="integer"),
     *                  @SWG\Property(property="name", type="string"),
     *                  @SWG\Property(property="courses", type="array", @SWG\Items(type="object")),
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function getCoursesTeaching(Request $request): JsonResponse
    {
        $widgets = $this->dashboardService->widgetsByCurrentUser();
        $organizationIds = !is_null($orgId = $request->input('organization_id'))
            ? $orgId
            : getOrganizationId(true);
        $response = array();

        $widgets->each(function (DashboardWidget $widget) use (&$response, $organizationIds) {
            switch ($widget->widget_slug) {
                case DashboardWidgetSlugsEnum::COURSES_TEACHING:
                    $response = [
                        'id' => $widget->id,
                        'name' => $widget->widget_name,
                        'courses' => $this->dashboardService->myCoursesTeaching($organizationIds),
                    ];
                    break;
            }
        });

        return $this->sendResponse($response, 'Courses teaching retrieved successfully');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @SWG\Get(
     *      path="/dashboard/assessments_due",
     *      summary="Get assesments by current teacher for dashboard",
     *      tags={"Dashboard"},
     *      description="Get data for dashboard",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="course_id",
     *          description="id of Course",
     *          type="integer",
     *          required=false,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="object",
     *                  @SWG\Property(property="id", type="integer"),
     *                  @SWG\Property(property="name", type="string"),
     *                  @SWG\Property(property="assessments", type="array", @SWG\Items(type="object")),
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function getAssessmentsByTeacher(Request $request): JsonResponse
    {
        $widget = $this->dashboardService->getMyWidgetBySlug(DashboardWidgetSlugsEnum::ASSESSMENTS_DUE_LIST);

        if (empty($widget)) {
            return $this->sendResponse([], 'Assessments due retrieved successfully');
        }

        $organizationIds = getOrganizationId(true);
        $courseId = $request->input('course_id');
        $response = [
            'id' => $widget->id,
            'name' => $widget->widget_name,
            'assessments' => $this->dashboardService->myAssessmentsDueByCourses($courseId, $organizationIds, $request),
        ];

        return $this->sendResponse($response, 'Assessments due retrieved successfully');
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @SWG\Get(
     *      path="/dashboard/student_statistic",
     *      summary="Get assesments by current teacher for dashboard",
     *      tags={"Dashboard"},
     *      description="Get data for dashboard",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="type",
     *          description="must be a week, day or month",
     *          type="string",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="object",
     *                  @SWG\Property(property="id", type="integer"),
     *                  @SWG\Property(property="name", type="string"),
     *                  @SWG\Property(property="data", type="array", @SWG\Items(type="object")),
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function getStudentStatistic(Request $request): JsonResponse
    {
        $widget = $this->dashboardService->getMyWidgetBySlug(DashboardWidgetSlugsEnum::STUDENT_STATISTICS);

        if (empty($widget)) {
            return $this->sendResponse([], 'Statistic retrieved successfully');
        }

        $organizationIds = !is_null($orgId = $request->input('organization_id'))
            ? $orgId
            : getOrganizationId(true);
        $frequencyType = $request->input('type');

        $response = [
            'id' => $widget->id,
            'name' => $widget->widget_name,
            'chart_data' => $this->dashboardService->studentStatistic($organizationIds, $frequencyType),
        ];

        return $this->sendResponse($response, 'Statistic retrieved successfully');
    }
}
