<?php

namespace App\Services\DashboardService;


use App\Models\Dashboard\DashboardWidget;
use App\Models\Role;
use App\Repositories\AssessmentRepository;
use App\Repositories\CourseRepository;
use App\Repositories\Dashboard\UserDashboardWidgetRepository;
use App\Repositories\LoginHistoryRepository;
use App\Repositories\StudentCourseRepository;
use App\Repositories\TeacherCourseRepository;
use App\Repositories\UserOrganizationRepository;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;

class DashboardService implements DashboardServiceInterface
{
    private $courseRepo;
    private $userOrganizationRepo;
    private $teacherCourseRepo;
    private $userDashboardWidgetRepo;
    private $assessmentRepo;
    private $studentCourseRepo;
    private $loginHistoryRepo;

    public function __construct(
        CourseRepository $courseRepo,
        UserOrganizationRepository $userOrganizationRepo,
        TeacherCourseRepository $teacherCourseRepo,
        UserDashboardWidgetRepository $userDashboardWidgetRepo,
        AssessmentRepository $assessmentRepo,
        StudentCourseRepository $studentCourseRepo,
        LoginHistoryRepository $loginHistoryRepo
    ) {
        $this->courseRepo = $courseRepo;
        $this->userOrganizationRepo = $userOrganizationRepo;
        $this->teacherCourseRepo = $teacherCourseRepo;
        $this->userDashboardWidgetRepo = $userDashboardWidgetRepo;
        $this->assessmentRepo = $assessmentRepo;
        $this->studentCourseRepo = $studentCourseRepo;
        $this->loginHistoryRepo = $loginHistoryRepo;
    }

    public function widgetsByCurrentUser()
    {
        // TODO uncomment when frontend done with this
//        $user = Auth::user();
//        return $user->dashboardWidgets()->get(['id', 'widget_name', 'widget_slug']);
        return DashboardWidget::all(['id', 'widget_name', 'widget_slug']);
    }

    public function getMyWidgetBySlug(string $slug)
    {
        // TODO uncomment when frontend done with this
//        $user = Auth::user();
//        return $user->dashboardWidgets()
//            ->where('widget_slug', $slug)
//            ->first(['id', 'widget_name', 'widget_slug']);
        return DashboardWidget::where('widget_slug', $slug)->first(['id', 'widget_name', 'widget_slug']);
    }

    public function coursesCount($organizationIds): int
    {
        return $this->courseRepo->getCoursesCountByOrganizationIds($organizationIds);
    }

    public function usersCountByRoleName(string $roleName, $organizationIds): int
    {
        $roleId = Role::where('name', $roleName)->value('id');

        return $this->userOrganizationRepo->getCountByOrgIdsAndRole($organizationIds, $roleId);
    }

    public function usersOnlineCountByRoleName(string $roleName, $organizationIds): int
    {
        $roleId = Role::where('name', $roleName)->value('id');

        return $this->userOrganizationRepo->getOnlineUsersCountByOrgIdAndRole($organizationIds, $roleId);
    }

    public function myTeachingCoursesCount($organizationIds): int
    {
        return $this->teacherCourseRepo->getCountCoursesByTeacherId(Auth::id(), $organizationIds);
    }

    public function myCoursesTeaching($organizationIds)
    {
        return $this->teacherCourseRepo->getCoursesByTeacherId(Auth::id(), $organizationIds);
    }

    public function myAssessmentsDueByCourses($courseIds, $organizationIds, $request)
    {
        if (empty($courseIds)) {
            $courseIds = $this->teacherCourseRepo->getCoursesByTeacherId(Auth::id(), $organizationIds, ['courses.id'])
                ->pluck('id')
                ->all();
        }

        return $this->assessmentRepo->getByCourseIdsForDashboard($courseIds, $organizationIds, $request);
    }

    public function studentStatistic($organizationIds, string $frequencyType = 'week'): array
    {
        list($startDatetime, $endDatetime) = $this->getPeriodByFrequencyType($frequencyType);

        $roleStudentId = Role::where('name', Role::ROLE_STUDENT)->value('id');

        $loginDates = $this->loginHistoryRepo
            ->getLoginsByOrganizationAndRole($organizationIds, $roleStudentId, $startDatetime, $endDatetime)
            ->pluck('created_at');
        $enrollDates = $this->studentCourseRepo
            ->getEnrolledStatistic($organizationIds, $startDatetime, $endDatetime)
            ->pluck('user_enrolled_date');

        $loginCounts = $this->countsByFrequencyAndDatesList($frequencyType, $loginDates);
        $enrollmentCounts = $this->countsByFrequencyAndDatesList($frequencyType, $enrollDates);

        $response = [];
        $response[] = $this->makeObjectForChart($loginCounts, 'Logins', '#FF6D48');
        $response[] = $this->makeObjectForChart($enrollmentCounts, 'Enrollments', '#FFCC32');
        $response[] = $this->makeObjectForChart([], 'Course completion', '#67B542');

        return $response;
    }


    /**
     * @param string $frequencyType must be 'week', 'day', 'month'...
     * @return array
     */
    private function getPeriodByFrequencyType(string $frequencyType = 'week'): array
    {
        $endDatetime = now();
        switch ($frequencyType) {
            case 'month':
                $startDatetime = $endDatetime->copy()->subMonth();
                $format = 'M d';
                break;
            case 'day':
                $startDatetime = $endDatetime->copy()->subDay();
                $format = 'M d H:00';
                break;
            default:
                $startDatetime = $endDatetime->copy()->subWeek();
                $format = 'M d';
                break;
        }

        return [$startDatetime, $endDatetime, $format];
    }

    /**
     * @param string $frequencyType must be 'week', 'day', 'month'...
     * @param $dateTimesCollection
     * @return array
     */
    private function countsByFrequencyAndDatesList(string $frequencyType, $dateTimesCollection): array
    {
        list($startDatetime, $endDatetime, $format) = $this->getPeriodByFrequencyType($frequencyType);
        $dateCounts = [];
        if ($frequencyType == 'day') {
            while ($endDatetime->timestamp >= $startDatetime->timestamp) {
                $dateCounts[$startDatetime->format($format)] = 0;
                $startDatetime->addHour();
            }
        } else {
            while ($endDatetime->timestamp >= $startDatetime->timestamp) {
                $dateCounts[$startDatetime->format($format)] = 0;
                $startDatetime->addDay();
            }
        }

        $dateTimesCollection->map(function (Carbon $dateTime) use ($frequencyType, &$dateCounts, $format){
            $date = $dateTime->format($format);
            if (isset($dateCounts[$date])) {
                $dateCounts[$date] ++;
            }
        });

        $dateObjects = [];
        foreach ($dateCounts as $date => $count) {
            $dateObjects[] = [
              'x' => $date,
              'y' => $count,
            ];
        }

        return $dateObjects;
    }

    private function makeObjectForChart(array $values, string $label, string $color): array
    {
        return [
            'data' => $values,
            'label' => $label,
            'pointBackgroundColor' => $color,
        ];
    }
}
